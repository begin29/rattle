module ApplicationHelper
  def erorr_for record, value
    record.errors.messages[value].first
  end
end
