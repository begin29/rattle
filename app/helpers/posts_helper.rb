module PostsHelper
  def vote_result votable
    votes = votable.votes
    like_count = votes.select{|v| v.value == true }.count
    dilike_count = votes.select{|v| v.value == false }.count
    # binding.pry
    "#{like_count}/#{dilike_count < 0 ? dilike_count : -dilike_count}"
  end
end
