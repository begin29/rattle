$( document ).ready(function() {

  $('body').on('click', 'a.edit', function(){
    var _this = this;
    $.get( $(this).attr('href'), function(data){
      $(_this).closest('.edited-area').html(data);
    });
    return false;
  });


  $( 'body' ).on( 'click', '.save', function(e) {
    e.preventDefault();
    var _this = this;
    var form = $(_this).closest('form');
    $.post( form.attr('action'), form.serialize(), function(data){
      $(_this).closest('.edited-area').html(data);
    });
  });

  $( 'body' ).on( 'click', '.save-comment', function(e) {
    e.preventDefault();
    var _this = this;
    var form = $(_this).closest('form');

    $.ajax({
      url: form.attr('action'),
      dataType: 'html',
      method: 'POST',
      success: function(resp) { comment_save_suc(_this, resp) },
      error: function(resp) { comment_save_err(_this, resp) },
      data: form.serialize()
    });
  });

  $( 'body' ).on( 'click', '.delete', function(e) {
    e.preventDefault();
    var _this = $(this);

    $.ajax({
      url: _this.attr('href'),
      type: 'DELETE',
      success: function(){
        $(_this).closest('.post-row').remove();
      }
    });
  });

  function comment_save_suc(_this, resp){
    var comment_counter = $(_this).closest('.post-row').find('.comment-counter span');
    var cc_value = comment_counter.html();
    comment_counter.html(++cc_value);

    $(_this).closest('.edited-area').html(resp);
  }

  function comment_save_err(_this, resp){
    $(_this).closest('.edited-area').html(resp.responseText);
  }

  $( 'body' ).on( 'click', '.cancel-comment', function(e) {
    e.preventDefault();
    $(this).closest('.edited-area').html('');
  });

  $( 'body' ).on( 'click', '.cancel', function(e) {
    e.preventDefault();
    var _this = this;
    $.get( $(_this).attr('href'), function(data){
      $(_this).closest('.edited-area').html(data);
    });
  });

  $('body').on('click', '.reply', function(){
    var _this = this;
    $.get( $(this).attr('href'), function(data){
      $(_this).closest('.edited-area').nextAll('.new-comment-area').html(data);
    });
    return false;
  });

  $('body').on('click', '.vote', function(e){
    e.preventDefault();
    var _this = this;
    $.post( $(_this).attr('href'), function(data){

      $(_this).closest('.vote-area').replaceWith(data);
    });
  });



})