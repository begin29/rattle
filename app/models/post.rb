class Post < ActiveRecord::Base
  belongs_to :user
  has_many :comments
  has_many :votes, as: :votable
  has_and_belongs_to_many :tags

  validates :title, presence: true
  validates :description, presence: true

  scope :scoped_for, ->(current_user){ joins(:tags).where( tags: {id: current_user.tag_ids}  ) if current_user }
  scope :for_chanel, ->(params){ joins(:tags).where( tags: { id: params[:chanel_id] } )  if params[:chanel_id]}
end
