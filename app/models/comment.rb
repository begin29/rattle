class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :parent, class_name: Comment
  belongs_to :post
  has_many :votes, as: :votable

  validates :text, presence: true

  scope :ordered_by_date, ->(){ order(created_at: :desc) }
end
