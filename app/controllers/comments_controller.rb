class CommentsController < ApplicationController

  def new
    @comment = Comment.new(post_id: params[:post_id])
    if params[:parent_id]

    else
      render :new, layout: false
    end
  end

  def show
    @comment = Comment.find params[:id]
    render layout: false
  end


  def create
    @comment = Comment.new comment_params
    if @comment.save
      render partial: 'show_comment', locals: { c: @comment, p: @comment.post }
    else
      render action: :new, layout: false, status: 400
    end

  end


  private

  def comment_params
    params.require(:comment).permit(:post_id, :parent_id, :text, :user_id)
  end
end