module PostConcern
  extend ActiveSupport::Concern

  def add_new_tags new_tags
    tags_array = new_tags.split(',').compact

    if tags_array.any?
      new_tags = []
      tags_array.each do |tag_name|
        stripped_name = tag_name.strip
        new_tags << Tag.create(name: stripped_name) unless Tag.where(name: stripped_name).any?
      end

      new_tags.map(&:id)
    end

  end

  # would be universal method for comment voting in future
  def recalculate_votes_for post_id, val
    @votes = Vote.where(votable_id: post_id,votable_type: 'Post', user_id: current_user.id)
    if @votes.any?
      @votes.destroy_all
      # binding.pry
    else
      Vote.create( user_id: current_user.id, value: val, votable_id: post_id,votable_type: 'Post')
    end
  end
end