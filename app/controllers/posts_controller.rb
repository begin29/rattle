class PostsController < ApplicationController
  include PostConcern
  require "pry"

  def index
    # NOTE: scoped_for method for future user subscription (see readme)
    # @posts = Post.scoped_for(current_user).page(params[:page])
    @posts = Post.page(params[:page])
  end

  def new
    @post = Post.new(user_id: current_user.id)
  end

  def show
    @post = Post.find params[:id]
    render layout: false
  end

  def create
    @post = Post.new post_params
    if @post.save
      redirect_to posts_path
    else
      render action: :new, layout: false
    end
  end

  def edit
    @post = Post.find(params[:id])
    render layout: false
  end

  def update
    @post = Post.find params[:id]
    new_tag_ids = add_new_tags(params[:new_tags])
    @post.tag_ids += new_tag_ids if new_tag_ids

    if @post.update_attributes post_params
      render "show", layout: false
    else
      render action: :edit
    end
  end

  def destroy
    @post = Post.find params[:id]
    @post.comments.destroy_all
    @post.destroy
    render nothing: true
  end

  def vote_up
    recalculate_votes_for params[:id], 1
    @post = Post.find(params[:id])
    render partial: "shared/reply_vote_path", locals: {votable: @post}
  end

  def vote_down
    recalculate_votes_for params[:id], -1
    @post = Post.find(params[:id])
    render partial: 'shared/reply_vote_path', locals: {votable: @post}
  end

  private

  def post_params
    params.require(:post).permit(:title, :description, :user_id, :tag_ids)
  end

end
