# Readme

### Getting started
```ruby
bundle install
rake db:create && db:migrate && db:seed
```

###Short explanation and thin moments
Rattle is a megacool site for creating own posts, vote and reply on comments.

Unimplemented features but with ready logic:

1) I've added Post model with field parent_id for future implement Reply functional for comments.

2) Added relation for User with Tags that can subscribe User to some chanell and show also posts from this chanel. (Just add tag to user and find all posts that have this tag)

3) Vote for comments. I've added polymorphic relation for Vote model with Post and Comment. Implemented only voting for Post. Logic for Comment is equeal.

Here is located general functional for Rattle and logic for potential new.
I am ready to talk with "customer" about details and future features =)