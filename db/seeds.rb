# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'pry'

data = YAML::load File.open('lib/tasks/initial_data.yml')

data['users'].each do |user_hash|
  User.create!(user_hash) unless User.where(email: user_hash['email']).any?
end

data['tags'].each do |tag_hash|
  Tag.create!(tag_hash) unless Tag.where(name: tag_hash['name']).any?
end

data['posts'].each do |post_hash|
  unless Post.where( title: post_hash['title'] ).any?
    owner_id = User.where(email: post_hash['owner_email']).first.try(:id)
    tag_ids = Tag.where(name: post_hash['tags']).map(&:id)
    hash = post_hash.except('tags', 'owner_email')
    hash.merge!(tag_ids: tag_ids, user_id: owner_id)
    Post.create!(hash)
  end
end

data['comments'].each do |comment_hash|
  post_id = Post.where(title: comment_hash['post_title']).first.try(&:id)
  user_id = User.where(email: comment_hash['owner_email']).first.try(&:id)

  unless Comment.where(post_id: post_id, user_id: user_id).any?
    hash = comment_hash.except('post_title', 'owner_email')
    hash.merge!(post_id: post_id, user_id: user_id)
    Comment.create!(hash)
  end
end

data['votes'].each do |vote_hash|
  votable_type = 'Post'
  votable_id = Post.where(title: vote_hash['post_title']).first.try(:id)
  user_id = User.where(email: vote_hash['user_email']).first.try(:id)

  unless Vote.where(votable_id: votable_id, user_id: user_id).any?
    hash = vote_hash.except('post_title', 'user_email')
    hash.merge!(votable_type: votable_type, votable_id: votable_id, user_id: user_id)
    Vote.create!(hash)
  end
end

# add tags to user like subscriptions
@user = User.first
@user.tag_ids = Tag.first(3).map(&:id) unless @user.tags.any?

